# portfolio
This is my portfolio.

## Sample
This sample is meant to show some of my skills.

## Step-by-step
1. Install dependencies with:
```
    pip install -r requirements.txt
```
2. Run define_best_startup_to_invest.py:
```
    python define_best_startup_to_invest.py
```
3. Read results:
```
Sampled: It is the sampled dependent variable
Predicted: The value predicted by the model
Error: The percentage distance from the predicted value to the sample value
```
## Release 1.0
Skills:
Git
.csv Manipulation
Dependency controlling
Data analysis
Simple Machine Learning (Polynomial Regression)